package com.banquemisr.irrigation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import com.banquemisr.irrigation.common.Utilities;
import com.fasterxml.jackson.core.JsonProcessingException;

@SpringBootTest
@DirtiesContext
public abstract class AbstractIT {

    protected MockMvc mvc;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    public static String mapToJson(Object obj) throws JsonProcessingException {
        return Utilities.getObjectMapper().writeValueAsString(obj);
    }

}
