package com.banquemisr.irrigation.repositories.plotland.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.banquemisr.irrigation.repositories.AbstractDocument;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document("plotLands")
public class PlotLandDocument extends AbstractDocument {

    @Id
    private String id;
    private String name;
    @Indexed(unique = true)
    private String code;
    private float waterAmount;
    private String agriculturalCrop;
    private float cultivatedArea;
    private float longitude;
    private float latitude;

    private List<String> slots;

    private List<String> sensors;
}
