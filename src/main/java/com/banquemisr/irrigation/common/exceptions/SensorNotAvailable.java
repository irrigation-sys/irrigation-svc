package com.banquemisr.irrigation.common.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FAILED_DEPENDENCY, reason = "Sensor unavailable")
public class SensorNotAvailable extends Exception {
    private static final long serialVersionUID = 7459962131571611029L;

    public SensorNotAvailable(String sensorCode) {
        super("Sensor unavailable with code: " + sensorCode);
    }
}