package com.banquemisr.irrigation.common.exceptions;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.mongodb.MongoWriteException;

@ControllerAdvice
public class ErrorAdvice {

    @ExceptionHandler(value = { MethodArgumentNotValidException.class })
    public ResponseEntity<List<ResponseMessage>> handleValidationHandler(MethodArgumentNotValidException e) {

        List<ResponseMessage> responseMessages = e.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> new ResponseMessage(fieldError.getField() + ": " + fieldError.getDefaultMessage()))
                .collect(Collectors.toList());

        if (responseMessages.isEmpty())
            responseMessages.add(new ResponseMessage("Arguments not valid"));

        return new ResponseEntity<>(responseMessages, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = { SensorNotAvailable.class })
    public ResponseEntity<ResponseMessage> sensorNotAvailable(SensorNotAvailable e) {
        return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.FAILED_DEPENDENCY);
    }

    @ExceptionHandler(value = { MongoWriteException.class })
    public ResponseEntity<ResponseMessage> mongoException(MongoWriteException e) {
        return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.FAILED_DEPENDENCY);
    }
}
