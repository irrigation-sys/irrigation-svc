package com.banquemisr.irrigation.resources.plotland.boundary;

import java.text.MessageFormat;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banquemisr.irrigation.common.exceptions.SensorNotAvailable;
import com.banquemisr.irrigation.integration.notification.NotificationFacade;
import com.banquemisr.irrigation.integration.sensors.SensorFacade;
import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.repositories.transactions.boundary.TransactionRepository;
import com.banquemisr.irrigation.repositories.transactions.entity.TransactionDocument;
import com.banquemisr.irrigation.resources.plotland.boundary.docs.IPlotLandIrrigationResource;
import com.banquemisr.irrigation.resources.plotland.entity.PlotLandIrrigationModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@CrossOrigin
@RequestMapping(value = "/plot-lands/{code}")
public class PlotLandIrrigationResource implements IPlotLandIrrigationResource {

    private final PlotLandRepository plotLandRepository;
    private final TransactionRepository transactionRepository;
    private final SensorFacade sensorFacade;
    private final NotificationFacade notificationFacade;

    public PlotLandIrrigationResource(PlotLandRepository plotLandRepository,
            TransactionRepository transactionRepository,
            SensorFacade sensorFacade,
            NotificationFacade notificationFacade) {
        this.plotLandRepository = plotLandRepository;
        this.transactionRepository = transactionRepository;
        this.sensorFacade = sensorFacade;
        this.notificationFacade = notificationFacade;
    }

    @PostMapping("/irrigate")
    public ResponseEntity<Void> irrigate(@Valid @RequestBody @NotBlank PlotLandIrrigationModel plotLandIrrigateModel,
            @PathVariable String code) {
        Optional<PlotLandDocument> plotLandDocumentOptional = plotLandRepository.findByCode(code);

        if (plotLandDocumentOptional.isPresent()) {
            PlotLandDocument plotLandDocument = plotLandDocumentOptional.get();

            plotLandDocument.getSensors().forEach(sensor -> {
                TransactionDocument transactionDocument = new TransactionDocument();
                transactionDocument.setPlotLandId(plotLandDocument.getId());
                transactionDocument.setSensorCode(sensor);
                transactionDocument.setSlot(plotLandIrrigateModel.getSlot());

                try {
                    sensorFacade.sendWaterAmount(sensor, plotLandDocument.getWaterAmount());

                    transactionDocument.setStatus(true);

                    log.info("*************Successfully sent request to sensor {}", sensor);
                } catch (SensorNotAvailable sensorNotAvailable) {
                    transactionDocument.setStatus(false);

                    notificationFacade.alert(
                            MessageFormat.format("*************Failed to send request to sensor {0}, {1}", sensor,
                                    sensorNotAvailable.getMessage()));
                }

                transactionRepository.save(transactionDocument);
            });
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
