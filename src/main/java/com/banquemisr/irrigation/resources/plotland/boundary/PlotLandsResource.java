package com.banquemisr.irrigation.resources.plotland.boundary;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.validation.Valid;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.banquemisr.irrigation.repositories.plotland.boundary.PlotLandRepository;
import com.banquemisr.irrigation.repositories.plotland.entity.PlotLandDocument;
import com.banquemisr.irrigation.resources.plotland.boundary.docs.IPlotLandsResource;
import com.banquemisr.irrigation.resources.plotland.control.PlotLandsCtrl;
import com.banquemisr.irrigation.resources.plotland.entity.PlotLandModel;

@RestController
@CrossOrigin
@RequestMapping(value = "/plot-lands")
public class PlotLandsResource implements IPlotLandsResource {

    private final PlotLandRepository plotLandRepository;
    private final PlotLandsCtrl plotLandsCtrl;

    public PlotLandsResource(PlotLandRepository plotLandRepository, PlotLandsCtrl plotLandsCtrl) {
        this.plotLandRepository = plotLandRepository;
        this.plotLandsCtrl = plotLandsCtrl;
    }

    @GetMapping("/")
    public ResponseEntity<CollectionModel<EntityModel<PlotLandModel>>> findAll() {
        List<PlotLandModel> plotLandModels = plotLandsCtrl.getPlotLandModels(plotLandRepository.findAll());

        List<EntityModel<PlotLandModel>> plotLandEntityModels = StreamSupport
                .stream(plotLandModels.spliterator(), false)
                .map(plotLand -> EntityModel.of(plotLand,
                        linkTo(methodOn(PlotLandsResource.class).findOneByCode(plotLand.getCode())).withSelfRel(),
                        linkTo(methodOn(PlotLandsResource.class).findAll()).withRel("plotLands")))
                .collect(Collectors.toList());

        return ResponseEntity.ok(
                CollectionModel.of(plotLandEntityModels,
                        linkTo(methodOn(PlotLandsResource.class).findAll()).withSelfRel()));
    }

    @GetMapping("/{code}")
    public ResponseEntity<EntityModel<PlotLandModel>> findOneByCode(@PathVariable String code) {
        return plotLandRepository.findByCode(code)
                .map(plotLand -> EntityModel.of(plotLandsCtrl.getPlotLandModel(plotLand),
                        linkTo(methodOn(PlotLandsResource.class).findOneByCode(plotLand.getCode())).withSelfRel(),
                        linkTo(methodOn(PlotLandsResource.class).findAll()).withRel("plotLands")))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping("/")
    public ResponseEntity<PlotLandModel> createPlotLands(@Valid @RequestBody PlotLandModel plotLand) {
        PlotLandDocument plotLandDocument = plotLandsCtrl.getPlotLandDocument(plotLand);
        plotLandDocument = plotLandRepository.save(plotLandDocument);

        PlotLandModel plotLandModel = plotLandsCtrl.getPlotLandModel(plotLandDocument);

        final URI uri = linkTo(methodOn(PlotLandsResource.class)
                .findOneByCode(plotLandModel.getCode())).toUri();
        return ResponseEntity.created(uri).body(plotLandModel);
    }

    @PutMapping("/{code}")
    public ResponseEntity<PlotLandModel> updatePlotLands(@Valid @RequestBody PlotLandModel plotLand,
            @PathVariable String code) {
        Optional<PlotLandDocument> plotLandDocumentOptional = plotLandRepository.findByCode(code);

        if (plotLandDocumentOptional.isPresent()) {
            PlotLandDocument plotLandDocument = plotLandsCtrl.getPlotLandDocument(plotLand);
            plotLandDocument = plotLandRepository.save(plotLandDocument);

            PlotLandModel plotLandModel = plotLandsCtrl.getPlotLandModel(plotLandDocument);
            return ResponseEntity.ok(plotLandModel);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
