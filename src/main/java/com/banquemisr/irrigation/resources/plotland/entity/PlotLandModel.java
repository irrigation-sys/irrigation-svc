package com.banquemisr.irrigation.resources.plotland.entity;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.banquemisr.irrigation.common.validation.IsAlphanumericSpace;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PlotLandModel implements Serializable {

    private String id;

    @NotBlank
    @IsAlphanumericSpace
    private String name;

    @NotBlank
    private String code;

    private float waterAmount;

    @NotBlank
    @IsAlphanumericSpace
    private String agriculturalCrop;

    private float cultivatedArea;

    private float longitude;
    private float latitude;

    private List<String> slots;

    private List<String> sensors;
}
