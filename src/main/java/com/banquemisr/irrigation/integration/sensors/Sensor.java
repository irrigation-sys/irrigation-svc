package com.banquemisr.irrigation.integration.sensors;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Sensor {

    private String code;
    private String integrationPoint;
    private float waterAmount;

    public Sensor(String code, String integrationPoint) {
        this.code = code;
        this.integrationPoint = integrationPoint;
    }
}
